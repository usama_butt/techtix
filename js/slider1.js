// global.js
// -----------------------------------------------------------------

var baesman_global = (function (me, $, Drupal, undefined) {
    me.name = 'baesman_global';

    // Size specific elements to the height of the viewport
    me.sizeToViewport = function sizeToViewport() {
        viewportHeight = $(window).height();
        window.viewportHeight = viewportHeight;
        setTimeout(function () {

            // On all pages except Home, add a margin-bottom
            var marginBottom = 60;
            if ($('body').hasClass('front') || $('body').hasClass('node-type-client')) {
                marginBottom = 0;
            }

            $('.viewport-height, .viewport-height .video').css('height', viewportHeight - marginBottom);
            $('.viewport-height video').css('minHeight', viewportHeight - marginBottom);
        }, 1);
    }

    me.getPlayerFromElement = function getPlayerFromElement(element) {
        if (element.find('video').length > 0) {
            var video = element.find('video')[0];
            return video;
        } else {
            return false;
        }
    }

    me.initContactTab = function initContactTab() {

        // Open
        $('#contact-tab').on('click', function () {
            $('html').addClass('show-global-contact-form');
        });

        // Close
        $('#form-close').on('click', function () {
            $('html').removeClass('show-global-contact-form');
        });
    }
    me.initStopScroll = function initStopScroll() {
        // Keep page from scrolling to top
        var scrollIt = 0;
        $(window).scroll(function () {
            if ($('html').hasClass('primary-nav-open') || $('html').hasClass('show-global-contact-form')) {
                $(window).scrollTop(scrollIt);
            }
            ;
        });
    }

    // Accepts an array of media query sizes (xs, sm, md, lg, xl) and
    // returns true if the current screen size is using that media query.
    me.checkMediaQuery = function checkMediaQuery(sizes) {
        var mediaQueryExists = false,
                currentQuery = window.getComputedStyle(document.querySelector('html'), ':before').getPropertyValue('content').replace(/\W/g, '');

        $.each(sizes, function (index, value) {
            if (currentQuery == value) {
                mediaQueryExists = true;
            }
        });
        return mediaQueryExists;
    }

    function init(context, settings) {

        me.sizeToViewport();
        $(window).on('resize', function () {
            me.sizeToViewport();
        });

        me.initContactTab();

        // Size the hero text
        $('.hero .title-baseline').fitText(.9, {minFontSize: '55px', maxFontSize: '131px'});

        me.initStopScroll();

    }
    ;

    Drupal.behaviors[me.name] = {
        attach: init
    };

    return me;
}(baesman_global || {}, jQuery, Drupal));
;







/*!
 AnythingSlider v1.9.4 minified using Google Closure Compiler
 Original by Chris Coyier: http://css-tricks.com
 Get the latest version: https://github.com/CSS-Tricks/AnythingSlider
 */
;
(function (d, l, n) {
    d.anythingSlider = function (m, p) {
        var a = this, b, k;
        a.el = m;
        a.$el = d(m).addClass("anythingBase").wrap('<div class="anythingSlider"><div class="anythingWindow" /></div>');
        a.$el.data("AnythingSlider", a);
        a.init = function () {
            a.options = b = d.extend({}, d.anythingSlider.defaults, p);
            a.initialized = !1;
            d.isFunction(b.onBeforeInitialize) && a.$el.bind("before_initialize", b.onBeforeInitialize);
            a.$el.trigger("before_initialize", a);
            d('\x3c!--[if lte IE 8]><script>jQuery("body").addClass("as-oldie");\x3c/script><![endif]--\x3e').appendTo("body").remove();
            a.$wrapper = a.$el.parent().closest("div.anythingSlider").addClass("anythingSlider-" + b.theme);
            a.$outer = a.$wrapper.parent();
            a.$window = a.$el.closest("div.anythingWindow");
            a.$win = d(l);
            a.$controls = d('<div class="anythingControls"></div>');
            a.$nav = d('<ul class="thumbNav"><li><a><span></span></a></li></ul>');
            a.$startStop = d('<a href="#" class="start-stop"></a>');
            (b.buildStartStop || b.buildNavigation) && a.$controls.appendTo(b.appendControlsTo && d(b.appendControlsTo).length ? d(b.appendControlsTo) : a.$wrapper);
            b.buildNavigation && a.$nav.appendTo(b.appendNavigationTo && d(b.appendNavigationTo).length ? d(b.appendNavigationTo) : a.$controls);
            b.buildStartStop && a.$startStop.appendTo(b.appendStartStopTo && d(b.appendStartStopTo).length ? d(b.appendStartStopTo) : a.$controls);
            a.runTimes = d(".anythingBase").length;
            a.regex = b.hashTags ? RegExp("panel" + a.runTimes + "-(\\d+)", "i") : null;
            1 === a.runTimes && a.makeActive();
            a.flag = !1;
            b.autoPlayLocked && (b.autoPlay = !0);
            a.playing = b.autoPlay;
            a.slideshow = !1;
            a.hovered = !1;
            a.panelSize = [];
            a.currentPage = a.targetPage = b.startPanel = parseInt(b.startPanel, 10) || 1;
            b.changeBy = parseInt(b.changeBy, 10) || 1;
            k = (b.mode || "h").toLowerCase().match(/(h|v|f)/);
            k = b.vertical ? "v" : (k || ["h"])[0];
            b.mode = "v" === k ? "vertical" : "f" === k ? "fade" : "horizontal";
            "f" === k && (b.showMultiple = 1, b.infiniteSlides = !1);
            a.adj = b.infiniteSlides ? 0 : 1;
            a.adjustMultiple = 0;
            b.playRtl && a.$wrapper.addClass("rtl");
            b.buildStartStop && a.buildAutoPlay();
            b.buildArrows && a.buildNextBackButtons();
            a.$lastPage = a.$targetPage = a.$currentPage;
            if (b.expand) {
                if (!0 === b.aspectRatio)
                    b.aspectRatio = a.$el.width() / a.$el.height();
                else if ("string" === typeof b.aspectRatio && -1 !== b.aspectRatio.indexOf(":")) {
                    var c = b.aspectRatio.split(":");
                    b.aspectRatio = c[0] / c[1]
                }
                0 < b.aspectRatio && 1 < b.showMultiple && (b.aspectRatio *= b.showMultiple)
            }
            a.updateSlider();
            b.expand && (a.$window.css({width: "100%", height: "100%"}), a.checkResize());
            d.isFunction(d.easing[b.easing]) || (b.easing = "swing");
            b.pauseOnHover && a.$wrapper.hover(function () {
                a.playing && (a.$el.trigger("slideshow_paused", a), a.clearTimer(!0))
            }, function () {
                a.playing && (a.$el.trigger("slideshow_unpaused", a), a.startStop(a.playing, !0))
            });
            a.slideControls(!1);
            a.$wrapper.bind("mouseenter mouseleave", function (b) {
                d(this)["mouseenter" === b.type ? "addClass" : "removeClass"]("anythingSlider-hovered");
                a.hovered = "mouseenter" === b.type ? !0 : !1;
                a.slideControls(a.hovered)
            });
            d(n).keyup(function (c) {
                if (b.enableKeyboard && a.$wrapper.hasClass("activeSlider") && !c.target.tagName.match("TEXTAREA|INPUT|SELECT") && ("vertical" === b.mode || 38 !== c.which && 40 !== c.which))
                    switch (c.which) {
                        case 39:
                        case 40:
                            a.goForward();
                            break;
                        case 37:
                        case 38:
                            a.goBack()
                        }
            });
            a.currentPage = (b.hashTags ? a.gotoHash() : "") || b.startPanel || 1;
            a.gotoPage(a.currentPage, !1, null, -1);
            var f = "slideshow_resized slideshow_paused slideshow_unpaused slide_init slide_begin slideshow_stop slideshow_start initialized swf_completed".split(" ");
            d.each("onSliderResize onShowPause onShowUnpause onSlideInit onSlideBegin onShowStop onShowStart onInitialized onSWFComplete".split(" "), function (c, h) {
                d.isFunction(b[h]) && a.$el.bind(f[c], b[h])
            });
            d.isFunction(b.onSlideComplete) && a.$el.bind("slide_complete", function () {
                setTimeout(function () {
                    b.onSlideComplete(a)
                }, 0);
                return!1
            });
            a.initialized = !0;
            a.$el.trigger("initialized", a);
            a.startStop(b.autoPlay)
        };
        a.updateSlider = function () {
            a.$el.children(".cloned").remove();
            a.navTextVisible = "hidden" !== a.$nav.find("span:first").css("visibility");
            a.$nav.empty();
            a.currentPage = a.currentPage || 1;
            a.$items = a.$el.children();
            a.pages = a.$items.length;
            a.dir = "vertical" === b.mode ? "top" : "left";
            b.showMultiple = parseInt(b.showMultiple, 10) || 1;
            b.navigationSize = !1 === b.navigationSize ? 0 : parseInt(b.navigationSize, 10) || 0;
            a.$items.find("a").unbind("focus.AnythingSlider").bind("focus.AnythingSlider", function (c) {
                var f = d(this).closest(".panel"), f = a.$items.index(f) + a.adj;
                a.$items.find(".focusedLink").removeClass("focusedLink");
                d(this).addClass("focusedLink");
                a.$window.scrollLeft(0).scrollTop(0);
                -1 !== f && (f >= a.currentPage + b.showMultiple || f < a.currentPage) && (a.gotoPage(f), c.preventDefault())
            });
            1 < b.showMultiple && (b.showMultiple > a.pages && (b.showMultiple = a.pages), a.adjustMultiple = b.infiniteSlides && 1 < a.pages ? 0 : b.showMultiple - 1);
            a.$controls.add(a.$nav).add(a.$startStop).add(a.$forward).add(a.$back)[1 >= a.pages ? "hide" : "show"]();
            1 < a.pages && a.buildNavigation();
            "fade" !== b.mode && b.infiniteSlides && 1 < a.pages && (a.$el.prepend(a.$items.filter(":last").clone().addClass("cloned")), 1 < b.showMultiple ? a.$el.append(a.$items.filter(":lt(" + b.showMultiple + ")").clone().addClass("cloned multiple")) : a.$el.append(a.$items.filter(":first").clone().addClass("cloned")), a.$el.find(".cloned").each(function () {
                d(this).find("a,input,textarea,select,button,area,form").attr({disabled: "disabled", name: ""});
                d(this).find("[id]")[d.fn.addBack ? "addBack" : "andSelf"]().removeAttr("id")
            }));
            a.$items = a.$el.addClass(b.mode).children().addClass("panel");
            a.setDimensions();
            b.resizeContents ? (a.$items.css("width", a.width), a.$wrapper.css("width", a.getDim(a.currentPage)[0]).add(a.$items).css("height", a.height)) : a.$win.load(function () {
                a.setDimensions();
                k = a.getDim(a.currentPage);
                a.$wrapper.css({width: k[0], height: k[1]});
                a.setCurrentPage(a.currentPage, !1)
            });
            a.currentPage > a.pages && (a.currentPage = a.pages);
            a.setCurrentPage(a.currentPage, !1);
            a.$nav.find("a").eq(a.currentPage - 1).addClass("cur");
            "fade" === b.mode && (k = a.$items.eq(a.currentPage - 1), b.resumeOnVisible ? k.css({opacity: 1, visibility: "visible"}).siblings().css({opacity: 0, visibility: "hidden"}) : (a.$items.css("opacity", 1), k.fadeIn(0).siblings().fadeOut(0)))
        };
        a.buildNavigation = function () {
            if (b.buildNavigation && 1 < a.pages) {
                var c, f, e, h, g;
                a.$items.filter(":not(.cloned)").each(function (q) {
                    g = d("<li/>");
                    e = q + 1;
                    f = (1 === e ? " first" : "") + (e === a.pages ? " last" : "");
                    c = '<a class="panel' + e + (a.navTextVisible ? '"' : " " + b.tooltipClass + '" title="@"') + ' href="#"><span>@</span></a>';
                    d.isFunction(b.navigationFormatter) ? (h = b.navigationFormatter(e, d(this)), "string" === typeof h ? g.html(c.replace(/@/g, h)) : g = d("<li/>", h)) : g.html(c.replace(/@/g, e));
                    g.appendTo(a.$nav).addClass(f).data("index", e)
                });
                a.$nav.children("li").bind(b.clickControls, function (c) {
                    !a.flag && b.enableNavigation && (a.flag = !0, setTimeout(function () {
                        a.flag = !1
                    }, 100), a.gotoPage(d(this).data("index")));
                    c.preventDefault()
                });
                b.navigationSize && b.navigationSize < a.pages && (a.$controls.find(".anythingNavWindow").length || a.$nav.before('<ul><li class="prev"><a href="#"><span>' + b.backText + "</span></a></li></ul>").after('<ul><li class="next"><a href="#"><span>' + b.forwardText + "</span></a></li></ul>").wrap('<div class="anythingNavWindow"></div>'), a.navWidths = a.$nav.find("li").map(function () {
                    return d(this).outerWidth(!0) + Math.ceil(parseInt(d(this).find("span").css("left"), 10) / 2 || 0)
                }).get(), a.navLeft = a.currentPage, a.$nav.width(a.navWidth(1, a.pages + 1) + 25), a.$controls.find(".anythingNavWindow").width(a.navWidth(1, b.navigationSize + 1)).end().find(".prev,.next").bind(b.clickControls, function (c) {
                    a.flag || (a.flag = !0, setTimeout(function () {
                        a.flag = !1
                    }, 200), a.navWindow(a.navLeft + b.navigationSize * (d(this).is(".prev") ? -1 : 1)));
                    c.preventDefault()
                }))
            }
        };
        a.navWidth = function (b, f) {
            var e;
            e = Math.min(b, f);
            for (var d = Math.max(b, f), g = 0; e < d; e++)
                g += a.navWidths[e - 1] || 0;
            return g
        };
        a.navWindow = function (c) {
            if (b.navigationSize && b.navigationSize < a.pages && a.navWidths) {
                var f = a.pages - b.navigationSize + 1;
                c = 1 >= c ? 1 : 1 < c && c < f ? c : f;
                c !== a.navLeft && (a.$controls.find(".anythingNavWindow").animate({scrollLeft: a.navWidth(1, c), width: a.navWidth(c, c + b.navigationSize)}, {queue: !1, duration: b.animationTime}), a.navLeft = c)
            }
        };
        a.buildNextBackButtons = function () {
            a.$forward = d('<span class="arrow forward"><a href="#"><span>' + b.forwardText + "</span></a></span>");
            a.$back = d('<span class="arrow back"><a href="#"><span>' + b.backText + "</span></a></span>");
            a.$back.bind(b.clickBackArrow, function (c) {
                b.enableArrows && !a.flag && (a.flag = !0, setTimeout(function () {
                    a.flag = !1
                }, 100), a.goBack());
                c.preventDefault()
            });
            a.$forward.bind(b.clickForwardArrow, function (c) {
                b.enableArrows && !a.flag && (a.flag = !0, setTimeout(function () {
                    a.flag = !1
                }, 100), a.goForward());
                c.preventDefault()
            });
            a.$back.add(a.$forward).find("a").bind("focusin focusout", function () {
                d(this).toggleClass("hover")
            });
            a.$back.appendTo(b.appendBackTo && d(b.appendBackTo).length ? d(b.appendBackTo) : a.$wrapper);
            a.$forward.appendTo(b.appendForwardTo && d(b.appendForwardTo).length ? d(b.appendForwardTo) : a.$wrapper);
            a.arrowWidth = a.$forward.width();
            a.arrowRight = parseInt(a.$forward.css("right"), 10);
            a.arrowLeft = parseInt(a.$back.css("left"), 10)
        };
        a.buildAutoPlay = function () {
            a.$startStop.html("<span>" + (a.playing ? b.stopText : b.startText) + "</span>").bind(b.clickSlideshow, function (c) {
                b.enableStartStop && (a.startStop(!a.playing), a.makeActive(), a.playing && !b.autoPlayDelayed && a.goForward(!0, b.playRtl));
                c.preventDefault()
            }).bind("focusin focusout", function () {
                d(this).toggleClass("hover")
            })
        };
        a.checkResize = function (b) {
            var f = !!(n.hidden || n.webkitHidden || n.mozHidden || n.msHidden);
            clearTimeout(a.resizeTimer);
            a.resizeTimer = setTimeout(function () {
                var e = a.$outer.width(), d = "BODY" === a.$outer[0].tagName ? a.$win.height() : a.$outer.height();
                f || a.lastDim[0] === e && a.lastDim[1] === d || (a.setDimensions(), a.$el.trigger("slideshow_resized", a), a.gotoPage(a.currentPage, a.playing, null, -1));
                "undefined" === typeof b && a.checkResize()
            }, f ? 2E3 : 500)
        };
        a.setDimensions = function () {
            a.$wrapper.find(".anythingWindow, .anythingBase, .panel")[d.fn.addBack ? "addBack" : "andSelf"]().css({width: "", height: ""});
            a.width = a.$el.width();
            a.height = a.$el.height();
            a.outerPad = [a.$wrapper.innerWidth() - a.$wrapper.width(), a.$wrapper.innerHeight() - a.$wrapper.height()];
            var c, f, e, h, g = 0, m = {width: "100%", height: "100%"}, k = 1 < b.showMultiple && "horizontal" === b.mode ? a.width || a.$window.width() / b.showMultiple : a.$window.width(), n = 1 < b.showMultiple && "vertical" === b.mode ? a.height / b.showMultiple || a.$window.height() / b.showMultiple : a.$window.height();
            if (b.expand) {
                a.lastDim = [a.$outer.width(), a.$outer.height()];
                c = a.lastDim[0] - a.outerPad[0];
                f = a.lastDim[1] - a.outerPad[1];
                if (b.aspectRatio && b.aspectRatio < a.width) {
                    var l = f * b.aspectRatio;
                    l < c ? c = l : (l = c / b.aspectRatio, l < f && (f = l))
                }
                a.$wrapper.add(a.$window).css({width: c, height: f});
                a.height = f = 1 < b.showMultiple && "vertical" === b.mode ? n : f;
                a.width = k = 1 < b.showMultiple && "horizontal" === b.mode ? c / b.showMultiple : c;
                a.$items.css({width: k, height: n})
            }
            a.$items.each(function (l) {
                h = d(this);
                e = h.children();
                b.resizeContents ? (c = a.width, f = a.height, h.css({width: c, height: f}), e.length && ("EMBED" === e[0].tagName && e.attr(m), "OBJECT" === e[0].tagName && e.find("embed").attr(m), 1 === e.length && e.css(m))) : ("vertical" === b.mode ? (c = h.css("display", "inline-block").width(), h.css("display", "")) : c = h.width() || a.width, 1 === e.length && c >= k && (c = e.width() >= k ? k : e.width(), e.css("max-width", c)), h.css({width: c, height: ""}), f = 1 === e.length ? e.outerHeight(!0) : h.height(), f <= a.outerPad[1] && (f = a.height), h.css("height", f));
                a.panelSize[l] = [c, f, g];
                g += "vertical" === b.mode ? f : c
            });
            a.$el.css("vertical" === b.mode ? "height" : "width", "fade" === b.mode ? a.width : g)
        };
        a.getDim = function (c) {
            var f, e, d = a.width, g = a.height;
            if (1 > a.pages || isNaN(c))
                return[d, g];
            c = b.infiniteSlides && 1 < a.pages ? c : c - 1;
            if (e = a.panelSize[c])
                d = e[0] || d, g = e[1] || g;
            if (1 < b.showMultiple)
                for (e = 1; e < b.showMultiple; e++)
                    f = c + e, "vertical" === b.mode ? (d = Math.max(d, a.panelSize[f][0]), g += a.panelSize[f][1]) : (d += a.panelSize[f][0], g = Math.max(g, a.panelSize[f][1]));
            return[d, g]
        };
        a.goForward = function (c, d) {
            a.gotoPage(a[b.allowRapidChange ? "targetPage" : "currentPage"] + b.changeBy * (d ? -1 : 1), c)
        };
        a.goBack = function (c) {
            a.gotoPage(a[b.allowRapidChange ? "targetPage" : "currentPage"] - b.changeBy, c)
        };
        a.gotoPage = function (c, f, e, h) {
            !0 !== f && (f = !1, a.startStop(!1), a.makeActive());
            /^[#|.]/.test(c) && d(c).length && (c = d(c).closest(".panel").index() + a.adj);
            if (1 !== b.changeBy) {
                var g = a.pages - a.adjustMultiple;
                1 > c && (c = b.stopAtEnd ? 1 : b.infiniteSlides ? a.pages + c : b.showMultiple > 1 - c ? 1 : g);
                c > a.pages ? c = b.stopAtEnd ? a.pages : b.showMultiple > 1 - c ? 1 : c -= g : c >= g && (c = g)
            }
            1 >= a.pages || (a.$lastPage = a.$currentPage, "number" !== typeof c && (c = parseInt(c, 10) || b.startPanel, a.setCurrentPage(c)), f && b.isVideoPlaying(a) || (b.stopAtEnd && !b.infiniteSlides && c > a.pages - b.showMultiple && (c = a.pages - b.showMultiple + 1), a.exactPage = c, c > a.pages + 1 - a.adj && (c = b.infiniteSlides || b.stopAtEnd ? a.pages : 1), c < a.adj && (c = b.infiniteSlides || b.stopAtEnd ? 1 : a.pages), b.infiniteSlides || (a.exactPage = c), a.currentPage = c > a.pages ? a.pages : 1 > c ? 1 : a.currentPage, a.$currentPage = a.$items.eq(a.currentPage - a.adj), a.targetPage = 0 === c ? a.pages : c > a.pages ? 1 : c, a.$targetPage = a.$items.eq(a.targetPage - a.adj), h = "undefined" !== typeof h ? h : b.animationTime, 0 <= h && a.$el.trigger("slide_init", a), 0 < h && !0 === b.toggleControls && a.slideControls(!0), b.buildNavigation && a.setNavigation(a.targetPage), !0 !== f && (f = !1), (!f || b.stopAtEnd && c === a.pages) && a.startStop(!1), 0 <= h && a.$el.trigger("slide_begin", a), setTimeout(function (d) {
                var f, g = !0;
                b.allowRapidChange && a.$wrapper.add(a.$el).add(a.$items).stop(!0, !0);
                b.resizeContents || (f = a.getDim(c), d = {}, a.$wrapper.width() !== f[0] && (d.width = f[0] || a.width, g = !1), a.$wrapper.height() !== f[1] && (d.height = f[1] || a.height, g = !1), g || a.$wrapper.filter(":not(:animated)").animate(d, {queue: !1, duration: 0 > h ? 0 : h, easing: b.easing}));
                "fade" === b.mode ? a.$lastPage[0] !== a.$targetPage[0] ? (a.fadeIt(a.$lastPage, 0, h), a.fadeIt(a.$targetPage, 1, h, function () {
                    a.endAnimation(c, e, h)
                })) : a.endAnimation(c, e, h) : (d = {}, d[a.dir] = -a.panelSize[b.infiniteSlides && 1 < a.pages ? c : c - 1][2], "vertical" !== b.mode || b.resizeContents || (d.width = f[0]), a.$el.filter(":not(:animated)").animate(d, {queue: !1, duration: 0 > h ? 0 : h, easing: b.easing, complete: function () {
                        a.endAnimation(c, e, h)
                    }}))
            }, parseInt(b.delayBeforeAnimate, 10) || 0)))
        };
        a.endAnimation = function (c, d, e) {
            0 === c ? (a.$el.css(a.dir, "fade" === b.mode ? 0 : -a.panelSize[a.pages][2]), c = a.pages) : c > a.pages && (a.$el.css(a.dir, "fade" === b.mode ? 0 : -a.panelSize[1][2]), c = 1);
            a.exactPage = c;
            a.setCurrentPage(c, !1);
            "fade" === b.mode && a.fadeIt(a.$items.not(":eq(" + (c - a.adj) + ")"), 0, 0);
            a.hovered || a.slideControls(!1);
            b.hashTags && a.setHash(c);
            0 <= e && a.$el.trigger("slide_complete", a);
            "function" === typeof d && d(a);
            b.autoPlayLocked && !a.playing && setTimeout(function () {
                a.startStop(!0)
            }, b.resumeDelay - (b.autoPlayDelayed ? b.delay : 0))
        };
        a.fadeIt = function (a, f, e, h) {
            var g = a.filter(":not(:animated)");
            a = 0 > e ? 0 : e;
            if (b.resumeOnVisible)
                1 === f && g.css("visibility", "visible"), g.fadeTo(a, f, function () {
                    0 === f && g.css("visibility", "hidden");
                    d.isFunction(h) && h()
                });
            else
                g[0 === f ? "fadeOut" : "fadeIn"](a, h)
        };
        a.setCurrentPage = function (c, d) {
            c = parseInt(c, 10);
            if (!(1 > a.pages || 0 === c || isNaN(c))) {
                c > a.pages + 1 - a.adj && (c = a.pages - a.adj);
                c < a.adj && (c = 1);
                b.buildArrows && !b.infiniteSlides && b.stopAtEnd && (a.$forward[c === a.pages - a.adjustMultiple ? "addClass" : "removeClass"]("disabled"), a.$back[1 === c ? "addClass" : "removeClass"]("disabled"), c === a.pages && a.playing && a.startStop());
                if (!d) {
                    var e = a.getDim(c);
                    a.$wrapper.css({width: e[0], height: e[1]}).add(a.$window).scrollLeft(0).scrollTop(0);
                    a.$el.css(a.dir, "fade" === b.mode ? 0 : -a.panelSize[b.infiniteSlides && 1 < a.pages ? c : c - 1][2])
                }
                a.currentPage = c;
                a.$currentPage = a.$items.removeClass("activePage").eq(c - a.adj).addClass("activePage");
                b.buildNavigation && a.setNavigation(c)
            }
        };
        a.setNavigation = function (b) {
            a.$nav.find(".cur").removeClass("cur").end().find("a").eq(b - 1).addClass("cur")
        };
        a.makeActive = function () {
            a.$wrapper.hasClass("activeSlider") || (d(".activeSlider").removeClass("activeSlider"), a.$wrapper.addClass("activeSlider"))
        };
        a.gotoHash = function () {
            var c = l.location.hash, f = c.indexOf("&"), e = c.match(a.regex);
            null !== e || /^#&/.test(c) || /#!?\//.test(c) || /\=/.test(c) ? null !== e && (e = b.hashTags ? parseInt(e[1], 10) : null) : (c = c.substring(0, 0 <= f ? f : c.length), e = d(c).length && d(c).closest(".anythingBase")[0] === a.el ? a.$items.index(d(c).closest(".panel")) + a.adj : null);
            return e
        };
        a.setHash = function (b) {
            var d = "panel" + a.runTimes + "-", e = l.location.hash;
            "undefined" !== typeof e && (l.location.hash = 0 < e.indexOf(d) ? e.replace(a.regex, d + b) : e + "&" + d + b)
        };
        a.slideControls = function (c) {
            var d = c ? "slideDown" : "slideUp", e = c ? 0 : b.animationTime, h = c ? b.animationTime : 0, g = c ? 1 : 0;
            c = c ? 0 : 1;
            b.toggleControls && a.$controls.stop(!0, !0).delay(e)[d](b.animationTime / 2).delay(h);
            b.buildArrows && b.toggleArrows && (!a.hovered && a.playing && (c = 1, g = 0), a.$forward.stop(!0, !0).delay(e).animate({right: a.arrowRight + c * a.arrowWidth, opacity: g}, b.animationTime / 2), a.$back.stop(!0, !0).delay(e).animate({left: a.arrowLeft + c * a.arrowWidth, opacity: g}, b.animationTime / 2))
        };
        a.clearTimer = function (b) {
            a.timer && (l.clearInterval(a.timer), !b && a.slideshow && (a.$el.trigger("slideshow_stop", a), a.slideshow = !1))
        };
        a.startStop = function (c, d) {
            !0 !== c && (c = !1);
            (a.playing = c) && !d && (a.$el.trigger("slideshow_start", a), a.slideshow = !0);
            b.buildStartStop && (a.$startStop.toggleClass("playing", c).find("span").html(c ? b.stopText : b.startText), "hidden" === a.$startStop.find("span").css("visibility") && a.$startStop.addClass(b.tooltipClass).attr("title", c ? b.stopText : b.startText));
            c ? (a.clearTimer(!0), a.timer = l.setInterval(function () {
                n.hidden || n.webkitHidden || n.mozHidden || n.msHidden ? b.autoPlayLocked || a.startStop() : b.isVideoPlaying(a) ? b.resumeOnVideoEnd || a.startStop() : a.goForward(!0, b.playRtl)
            }, b.delay)) : a.clearTimer()
        };
        a.init()
    };
    d.anythingSlider.defaults = {theme: "default", mode: "horiz", expand: !1, resizeContents: !0, showMultiple: !1, easing: "swing", buildArrows: !0, buildNavigation: !0, buildStartStop: !0, toggleArrows: !1, toggleControls: !1, startText: "Start", stopText: "Stop", forwardText: "&raquo;", backText: "&laquo;", tooltipClass: "tooltip", enableArrows: !0, enableNavigation: !0, enableStartStop: !0, enableKeyboard: !0, startPanel: 1, changeBy: 1, hashTags: !0, infiniteSlides: !0, navigationFormatter: null, navigationSize: !1, autoPlay: !1, autoPlayLocked: !1, autoPlayDelayed: !1, pauseOnHover: !0, stopAtEnd: !1, playRtl: !1, delay: 3E3, resumeDelay: 15E3, animationTime: 600, delayBeforeAnimate: 0, clickForwardArrow: "click", clickBackArrow: "click", clickControls: "click focusin", clickSlideshow: "click", allowRapidChange: !1, resumeOnVideoEnd: !0, resumeOnVisible: !0, isVideoPlaying: function (d) {
            return!1
        }};
    d.fn.anythingSlider = function (m, l) {
        return this.each(function () {
            var a, b = d(this).data("AnythingSlider");
            (typeof m).match("object|undefined") ? b ? b.updateSlider() : new d.anythingSlider(this, m) : /\d/.test(m) && !isNaN(m) && b ? (a = "number" === typeof m ? m : parseInt(d.trim(m), 10), 1 <= a && a <= b.pages && b.gotoPage(a, !1, l)) : /^[#|.]/.test(m) && d(m).length && b.gotoPage(m, !1, l)
        })
    }
})(jQuery, window, document);
;
// section-team.js
// -----------------------------------------------------------------

(function ($) {

    Drupal.behaviors.baesman_section_team = {
        attach: function (context, settings) {

            initSliders();
            $(window).on('resize', function () {
                initSliders();
            });

        }
    };

    function initSliders() {

        if (baesman_global.checkMediaQuery(['md', 'lg', 'xl'])) {

            $('.team-slider').removeClass('mobile');

            $('#team-slide-back-btn').on('click', function () {
                $('#team-slider').data('AnythingSlider').goBack();
            });

            // Slider controls
            $('#slide-control .btn').on('click', function () {

                if ($(this).hasClass('team-slide-prev')) {
                    $('#team-slider').data('AnythingSlider').goBack();
                } else {
                    $('#team-slider').data('AnythingSlider').goForward();
                }

            });

            // Slide click control
            $(document).on('click', '#team-slider .slide', function () {
                $('#team-slider').data('AnythingSlider').gotoPage($(this).data('slide'));
            });

            // Size team member name
            $('#team .slide h3').fitText();

            $('#active-slider').anythingSlider({
                // *********** Appearance ***********
                // Theme name; choose from: minimalist-round, minimalist-square,
                // metallic, construction, cs-portfolio
                theme: 'default',
                // Set mode to "horizontal", "vertical" or "fade"
                // (only first letter needed); replaces vertical option
                mode: 'fade',
                // If true, the entire slider will expand to fit the parent element
                expand: true,
                // If true, solitary images/objects in the panel will expand to
                // fit the viewport
                resizeContents: true,
                // Set this value to a number and it will show that many slides at once
                showMultiple: 1,
                // Anything other than "linear" or "swing" requires the easing plugin
                easing: "swing",
                // If true, builds the forwards and backwards buttons
                buildArrows: false,
                // If true, builds a list of anchor links to link to each panel
                buildNavigation: false,
                // If true, builds the start/stop button
                buildStartStop: false,
                // Append forward arrow to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendFowardTo: null,
                // Append back arrow to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendBackTo: null,
                // Append controls (navigation + start-stop) to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendControlsTo: null,
                // Append navigation buttons to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendNavigationTo: null,
                // Append start-stop button to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendStartStopTo: null,
                // If true, side navigation arrows will slide out on
                // hovering & hide @ other times
                toggleArrows: false,
                // if true, slide in controls (navigation + play/stop button)
                // on hover and slide change, hide @ other times
                toggleControls: false,
                // Start button text
                startText: "Start",
                // Stop button text
                stopText: "Stop",
                // Link text used to move the slider forward
                // (hidden by CSS, replaced with arrow image)
                forwardText: "&raquo;",
                // Link text used to move the slider back
                // (hidden by CSS, replace with arrow image)
                backText: "&laquo;",
                // Class added to navigation & start/stop button
                // (text copied to title if it is hidden by a negative text indent)
                tooltipClass: 'tooltip',
                // if false, arrows will be visible, but not clickable.
                enableArrows: false,
                // if false, navigation links will still be visible, but not clickable.
                enableNavigation: false,
                // if false, the play/stop button will still be visible, but not
                // clickable. Previously "enablePlay"
                enableStartStop: false,
                // if false, keyboard arrow keys will not work for this slider.
                enableKeyboard: false,
                // *********** Navigation ***********
                // This sets the initial panel
                startPanel: 1,
                // Amount to go forward or back when changing panels.
                changeBy: 1,
                // Should links change the hashtag in the URL?
                hashTags: false,
                // if false, the slider will not wrap
                infiniteSlides: true,
                // Details at the top of the file on this use (advanced use)
                navigationFormatter: function (index, panel) {
                    // This is the default format (show just the panel index number)
                    return "" + index;
                },
                // Set this to the maximum number of visible navigation tabs;
                // false to disable
                navigationSize: false,
                // *********** Slideshow options ***********
                // If true, the slideshow will start running; replaces "startStopped" option
                autoPlay: false,
                // If true, user changing slides will not stop the slideshow
                autoPlayLocked: false,
                // If true, starting a slideshow will delay advancing slides; if false, the slider will immediately advance to the next slide when slideshow starts
                autoPlayDelayed: false,
                // If true & the slideshow is active, the slideshow will pause on hover
                pauseOnHover: false,
                // If true & the slideshow is active, the  slideshow will stop on the last page. This also stops the rewind effect  when infiniteSlides is false.
                stopAtEnd: false,
                // If true, the slideshow will move right-to-left
                playRtl: false,
                // *********** Times ***********
                // How long between slideshow transitions in AutoPlay mode (in milliseconds)
                delay: 3000,
                // Resume slideshow after user interaction, only if autoplayLocked is true (in milliseconds).
                resumeDelay: 15000,
                // How long the slideshow transition takes (in milliseconds)
                animationTime: 600,
                // How long to pause slide animation before going to the desired slide (used if you want your "out" FX to show).
                delayBeforeAnimate: 0,
                // *********** Callbacks ***********
                // Callback before the plugin initializes
                onBeforeInitialize: function (e, slider) {
                },
                // Callback when the plugin finished initializing
                onInitialized: function (e, slider) {
                },
                // Callback on slideshow start
                onShowStart: function (e, slider) {
                },
                // Callback after slideshow stops
                onShowStop: function (e, slider) {
                },
                // Callback when slideshow pauses
                onShowPause: function (e, slider) {
                },
                // Callback when slideshow unpauses - may not trigger
                // properly if user clicks on any controls
                onShowUnpause: function (e, slider) {
                },
                // Callback when slide initiates, before control animation
                onSlideInit: function (e, slider) {
                },
                // Callback before slide animates
                onSlideBegin: function (e, slider) {
                },
                // Callback when slide completes - no event variable!
                onSlideComplete: function (slider) {
                },
                // Callback when slider resizes
                onSliderResize: function (e, slider) {
                },
                // *********** Interactivity ***********
                // Event used to activate forward arrow functionality
                // (e.g. add jQuery mobile's "swiperight")
                clickForwardArrow: "click",
                // Event used to activate back arrow functionality
                // (e.g. add jQuery mobile's "swipeleft")
                clickBackArrow: "click",
                // Events used to activate navigation control functionality
                clickControls: "click focusin",
                // Event used to activate slideshow play/stop button
                clickSlideshow: "click",
                // *********** Video ***********
                // If true & the slideshow is active & a youtube video
                // is playing, it will pause the autoplay until the video
                // is complete
                resumeOnVideoEnd: true,
                // If true the video will resume playing (if previously
                // paused, except for YouTube iframe - known issue);
                // if false, the video remains paused.
                resumeOnVisible: true,
                // If your slider has an embedded object, the script will
                // automatically add a wmode parameter with this setting
                addWmodeToObject: "opaque",
                // return true if video is playing or false if not - used
                // by video extension
                isVideoPlaying: function (base) {
                    return false;
                }

            });

            $('#team-slider').anythingSlider({
                // *********** Appearance ***********
                // Theme name; choose from: minimalist-round, minimalist-square,
                // metallic, construction, cs-portfolio
                theme: 'default',
                // Set mode to "horizontal", "vertical" or "fade"
                // (only first letter needed); replaces vertical option
                mode: 'horizontal',
                // If true, the entire slider will expand to fit the parent element
                expand: true,
                // If true, solitary images/objects in the panel will expand to
                // fit the viewport
                resizeContents: true,
                // Set this value to a number and it will show that many slides at once
                showMultiple: 4,
                // Anything other than "linear" or "swing" requires the easing plugin
                easing: "swing",
                // If true, builds the forwards and backwards buttons
                buildArrows: false,
                // If true, builds a list of anchor links to link to each panel
                buildNavigation: false,
                // If true, builds the start/stop button
                buildStartStop: false,
                // Append forward arrow to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendFowardTo: null,
                // Append back arrow to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendBackTo: null,
                // Append controls (navigation + start-stop) to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendControlsTo: null,
                // Append navigation buttons to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendNavigationTo: null,
                // Append start-stop button to a HTML element
                // (jQuery Object, selector or HTMLNode), if not null
                appendStartStopTo: null,
                // If true, side navigation arrows will slide out on
                // hovering & hide @ other times
                toggleArrows: false,
                // if true, slide in controls (navigation + play/stop button)
                // on hover and slide change, hide @ other times
                toggleControls: false,
                // Start button text
                startText: "Start",
                // Stop button text
                stopText: "Stop",
                // Link text used to move the slider forward
                // (hidden by CSS, replaced with arrow image)
                forwardText: "&raquo;",
                // Link text used to move the slider back
                // (hidden by CSS, replace with arrow image)
                backText: "&laquo;",
                // Class added to navigation & start/stop button
                // (text copied to title if it is hidden by a negative text indent)
                tooltipClass: 'tooltip',
                // if false, arrows will be visible, but not clickable.
                enableArrows: false,
                // if false, navigation links will still be visible, but not clickable.
                enableNavigation: true,
                // if false, the play/stop button will still be visible, but not
                // clickable. Previously "enablePlay"
                enableStartStop: true,
                // if false, keyboard arrow keys will not work for this slider.
                enableKeyboard: true,
                // *********** Navigation ***********
                // This sets the initial panel
                startPanel: 1,
                // Amount to go forward or back when changing panels.
                changeBy: 1,
                // Should links change the hashtag in the URL?
                hashTags: false,
                // if false, the slider will not wrap
                infiniteSlides: true,
                // Details at the top of the file on this use (advanced use)
                navigationFormatter: function (index, panel) {
                    // This is the default format (show just the panel index number)
                    return "" + index;
                },
                // Set this to the maximum number of visible navigation tabs;
                // false to disable
                navigationSize: false,
                // *********** Slideshow options ***********
                // If true, the slideshow will start running; replaces "startStopped" option
                autoPlay: false,
                // If true, user changing slides will not stop the slideshow
                autoPlayLocked: false,
                // If true, starting a slideshow will delay advancing slides; if false, the slider will immediately advance to the next slide when slideshow starts
                autoPlayDelayed: false,
                // If true & the slideshow is active, the slideshow will pause on hover
                pauseOnHover: true,
                // If true & the slideshow is active, the  slideshow will stop on the last page. This also stops the rewind effect  when infiniteSlides is false.
                stopAtEnd: false,
                // If true, the slideshow will move right-to-left
                playRtl: false,
                // *********** Times ***********
                // How long between slideshow transitions in AutoPlay mode (in milliseconds)
                delay: 3000,
                // Resume slideshow after user interaction, only if autoplayLocked is true (in milliseconds).
                resumeDelay: 15000,
                // How long the slideshow transition takes (in milliseconds)
                animationTime: 600,
                // How long to pause slide animation before going to the desired slide (used if you want your "out" FX to show).
                delayBeforeAnimate: 0,
                // *********** Callbacks ***********
                // Callback before the plugin initializes
                onBeforeInitialize: function (e, slider) {
                },
                // Callback when the plugin finished initializing
                onInitialized: function (e, slider) {
                    $('#slide-control .slide-count-total').text(slider.pages);
                },
                // Callback on slideshow start
                onShowStart: function (e, slider) {
                },
                // Callback after slideshow stops
                onShowStop: function (e, slider) {
                },
                // Callback when slideshow pauses
                onShowPause: function (e, slider) {
                },
                // Callback when slideshow unpauses - may not trigger
                // properly if user clicks on any controls
                onShowUnpause: function (e, slider) {
                },
                // Callback when slide initiates, before control animation
                onSlideInit: function (e, slider) {
                },
                // Callback before slide animates
                onSlideBegin: function (e, slider) {
                    $('#active-slider').anythingSlider(slider.targetPage);
                    $('#slide-control .slide-count-active').text(slider.targetPage);
                },
                // Callback when slide completes - no event variable!
                onSlideComplete: function (slider) {
                },
                // Callback when slider resizes
                onSliderResize: function (e, slider) {
                },
                // *********** Interactivity ***********
                // Event used to activate forward arrow functionality
                // (e.g. add jQuery mobile's "swiperight")
                clickForwardArrow: "click",
                // Event used to activate back arrow functionality
                // (e.g. add jQuery mobile's "swipeleft")
                clickBackArrow: "click",
                // Events used to activate navigation control functionality
                clickControls: "click focusin",
                // Event used to activate slideshow play/stop button
                clickSlideshow: "click",
                // *********** Video ***********
                // If true & the slideshow is active & a youtube video
                // is playing, it will pause the autoplay until the video
                // is complete
                resumeOnVideoEnd: true,
                // If true the video will resume playing (if previously
                // paused, except for YouTube iframe - known issue);
                // if false, the video remains paused.
                resumeOnVisible: true,
                // If your slider has an embedded object, the script will
                // automatically add a wmode parameter with this setting
                addWmodeToObject: "opaque",
                // return true if video is playing or false if not - used
                // by video extension
                isVideoPlaying: function (base) {
                    return false;
                }

            });

        } else {

            $('.team-slider').addClass('mobile');

            // Overlay
            $(document).on('click', '.team-slider.mobile .slide', function () {

                $('.team-slider').addClass('active');

                // Clear out old overlay, grab the new one,
                // set it to the height of the slider section
                $('.team-slider > .overlay').remove();
                $(this).find('.overlay').clone().appendTo($('.team-slider'));
                $('.team-slider > .overlay').css('height', $('#team-slider').height());

                // Scroll to top of section
                $('html, body').animate({
                    scrollTop: $('#team-slider').offset().top - 50
                }, 800);
            });

            // Overlay - Close
            $(document).on('click', '#overlay-close,.team-slider.active .slides', function () {
                $('.team-slider').removeClass('active');
                $('.team-slider > .overlay').remove();
            });

        }

    }

})(jQuery);
;
